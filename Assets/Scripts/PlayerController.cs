﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Vector3 screenCentre;
    Vector3 orientationVector;
    float orientationAngle;

	// Use this for initialization
	void Start ()
    {
        screenCentre = new Vector3(Screen.width / 2, Screen.height / 2, 0);
	}

    // Update is called once per frame
    void Update ()
    {
        orientationVector = Input.mousePosition - screenCentre;

        orientationAngle = Mathf.Atan2(orientationVector.y, orientationVector.x) * Mathf.Rad2Deg - 90;
        transform.rotation = Quaternion.Euler(0, 0, orientationAngle);

        Debug.Log(orientationAngle);
    }
}
