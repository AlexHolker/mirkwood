﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{
    // Constants
    private int MAP_SIZE = 11;

    // Objects
    public GameObject tilePHFloor;
    public GameObject tilePHTree;
    public GameObject tilePHWall;
    /*public GameObject[] mapTilesCabin;
    public GameObject[] mapTilesForest;
    public GameObject[] mapTilesCave;
    public GameObject[] mapTilesLair;*/
    
    private List<GameObject> mapTileList = new List<GameObject>();
    private List<List<GameObject>> mapTileArray = new List<List<GameObject>>();

    // Create one-dimensional array of GameObjects representing the initial map of a cabin surrounded by snow.
    void InitialiseMapSetup()
    {
        Vector3 tilePosition;
        Quaternion tileRotation;
        GameObject tileType;

        mapTileArray.Clear();

        for (int x = 0; x < MAP_SIZE; x++)
        {
            mapTileList.Clear();

            for (int y = 0; y < MAP_SIZE; y++)
            {
                if (y < 2 || 8 < y)
                {
                    tileType = tilePHFloor;
                }
                else
                {
                    if (y < 3 || 7 < y)
                    {
                        tileType = tilePHWall;
                    }
                    else
                    {
                        tileType = tilePHFloor;
                    }
                }

                tilePosition = new Vector3(x-(MAP_SIZE-1)/2, y - (MAP_SIZE - 1) / 2, 0);
                tileRotation = Quaternion.identity;
                
                mapTileList.Add(Instantiate(tileType, tilePosition, tileRotation));
            }
            mapTileArray.Add(mapTileList);
        }
    }
    
	// Use this for initialization
	public void Start ()
    {
        InitialiseMapSetup();
	}
}
